import React,{Component} from 'react';
import styles from '../assets/css/Album.scss';
import Image from './Image';

export default class Album extends Component{
  constructor(props){
    super(props);

    this.state = {
        scrollWidthOfCarousel: 0,
        offsetWidthOfCarousel: 0,
        scrollLeftOfCarousel: 0,
        carouselId: 'carousel'+this.props.data.id
    };

    this.onScrollCarousel = this.onScrollCarousel.bind(this);
  }

  componentDidMount() {
      this.setState({offsetWidthOfCarousel: document.getElementById(this.state.carouselId).offsetWidth});
      this.setState({scrollLeftOfCarousel: document.getElementById(this.state.carouselId).scrollLeft});
      document.getElementById(this.state.carouselId).addEventListener('scroll',this.onScrollCarousel,true);

      // number of images i.e. length of images array in album data; here it is 10 hardcoded
      let numberOfImages = 10;
      let scrollWidthOfCarousel = numberOfImages*205-20;
      this.setState({scrollWidthOfCarousel});
  }


  onScrollCarousel(e) {
    this.setState({scrollLeftOfCarousel: e.target.scrollLeft});
  }

  scrollPrev() {
      if(typeof this.prevScroll === "undefined") {
          this.prevScroll = setInterval(()=> {
              document.getElementById(this.state.carouselId).scrollLeft = this.state.scrollLeftOfCarousel-20;
              this.setState({scrollLeftOfCarousel: this.state.scrollLeftOfCarousel-20});
          },50);
      }
  }

  stopPrevScroll() {
      clearInterval(this.prevScroll);
      this.prevScroll = undefined;
  }

  renderPrevScrollContainer() {
    if(this.state.scrollWidthOfCarousel > this.state.offsetWidthOfCarousel
        && this.state.scrollLeftOfCarousel > 0) {
        return(
          <div className="arrow-container"
               style={{left: 0}}
               onMouseOver={e=>this.scrollPrev()}
               onMouseOut={e=>this.stopPrevScroll()}>
            <div className="arrow-icon prev"></div>
          </div>
        )
    }else{
        clearInterval(this.prevScroll);
    }
  }

  scrollNext() {
      if(typeof this.nextScroll === "undefined") {
          this.nextScroll = setInterval(() => {
              document.getElementById(this.state.carouselId).scrollLeft = this.state.scrollLeftOfCarousel+20;
              this.setState({scrollLeftOfCarousel: this.state.scrollLeftOfCarousel + 20});
          }, 50);
      }
  }

  stopNextScroll() {
      clearInterval(this.nextScroll);
      this.nextScroll = undefined;
  }


  renderNextScrollContainer() {
    if(this.state.scrollWidthOfCarousel > this.state.offsetWidthOfCarousel
        && (this.state.scrollWidthOfCarousel-this.state.offsetWidthOfCarousel) > this.state.scrollLeftOfCarousel) {
        return(
          <div className="arrow-container"
               style={{right: 0}}
               onMouseOver={e=>this.scrollNext()}
               onMouseOut={e=>this.stopNextScroll()}>
            <div className="arrow-icon"></div>
          </div>
        )
    }else{
        clearInterval(this.nextScroll);
    }
  }


  renderImagesContainer() {
    if(typeof this.props.data.images !== "undefined") {
      if(this.props.data.images.length > 0) {
        return this.props.data.images.map((data,i)=> {
          if(i < 10) {
            return <Image key={i}
                          data={data}/>;
          }
        });
      }
    }
  }

  renderAlbumIdAndUseId() {
    return "id: "+this.props.data.id+", userid: "+this.props.data.userId;
  }

  render(){
    return(
      <div className="album-container">
        <div className="album-upper-container">
          <div className="album-title">{this.props.data.title}</div>
          <div className="album-details">{this.renderAlbumIdAndUseId()}</div>
        </div>
        <div className="album-lower-container">
          {this.renderPrevScrollContainer()}
          <div className="carousel-container" id={this.state.carouselId}>
            {this.renderImagesContainer()}
          </div>
          {this.renderNextScrollContainer()}
        </div>
      </div>
    )
  }
}
