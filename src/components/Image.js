import React,{Component} from 'react';
import styles from '../assets/css/Image.scss';

export default class Image extends Component{
  constructor(props){
    super(props);
  }

  renderAlbumIdAndUseId() {
    return "id: "+this.props.data.id+", userid: "+this.props.data.userId;
  }

  renderImageId() {
    return "id: "+ this.props.data.id;
  }

  render(){
    let imageStyle = {
        background: 'url(' +this.props.data.url+ ')',
        backgroundSize: '100% 100%'
    };

    return(
      <div className="image-container">
        <div className="image-sub-container">
          <div className="url-image" style={imageStyle}></div>
          <div className="image-title">{this.props.data.title}</div>
          <div className="image-id">{this.renderImageId()}</div>
        </div>
      </div>
    )
  }
}
