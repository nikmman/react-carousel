import React,{Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import styles from '../assets/css/App.scss';

import {reportApiCall} from '../rdx/actions';
import {GET_ALBUMS} from '../helpers/APITypes';
import {selectListOfAlbums} from '../rdx/selectors/selectors';
import Album from './Album';

class App extends Component{
  constructor(props){
    super(props);
  }

  componentDidMount() {
    this.props.reportApiCall(GET_ALBUMS);
  }

  renderListOfAlbums() {
    if(this.props.listOfAlbums.length > 0) {
      return this.props.listOfAlbums.map((data,i)=> {
        if(i < 10) {
          return <Album key={i}
                        data={data} />;
        }
      });
    }
  }

  render(){
    return(
      <div className="container">
        {this.renderListOfAlbums()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return{
    listOfAlbums: selectListOfAlbums(state)
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    reportApiCall
  },dispatch);
};

export default connect(mapStateToProps,mapDispatchToProps)(App);
