export function makeURLFromData(baseURL, data) {
    let url = baseURL+'?';
    Object.keys(data).forEach((key, value) => {
        if(value == 0) {
            url = url.concat(key+"="+data[key]);
        }else {
            url = url.concat("&"+key+"="+data[key]);
        }
    });
    return url.replace(/\+/g , "%2B");
}
