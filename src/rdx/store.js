import {createStore} from 'redux';
import reducers from './reducers/reducers.js';

export const initStore = () => {
  return createStore(reducers);
};
