import {
  REPORT_API_CALL,
  REPORT_ALBUMS,
  REPORT_IMAGES_OF_ALBUM
} from '../constants/action-types.js';
import {combineReducers} from 'redux';

export const callAPI = (state={},action) => {
    switch (action.type){
        case REPORT_API_CALL:
            return Object.assign({},action.payload);
    }
    return state;
};

export const listOfAlbums = (state=[],action) => {
  switch (action.type) {
    case REPORT_ALBUMS:
      return action.payload;
    case REPORT_IMAGES_OF_ALBUM:
      let stateClone = [...state];
      let albumData = stateClone[action.payload.albumId-1];
      albumData['images'] = action.payload.images;
      stateClone[action.payload.albumId-1] = albumData;
      return stateClone;
  }
  return state;
};


export default combineReducers({
  callAPI,
  listOfAlbums
});
