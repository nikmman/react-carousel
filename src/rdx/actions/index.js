import {
  REPORT_API_CALL,
  REPORT_ALBUMS,
  REPORT_IMAGES_OF_ALBUM
} from '../constants/action-types.js';

export const reportApiCall = (type, data) => {
    return {
        type: REPORT_API_CALL,
        payload: {
            type,
            data
        }
    }
};

export const reportAlbums = (albums) => {
  return {
    type: REPORT_ALBUMS,
    payload: albums
  }
};

export const reportImagesOfAlbum = (albumId, images) => {
  return {
    type: REPORT_IMAGES_OF_ALBUM,
    payload: {
      albumId,
      images
    }
  }
};
