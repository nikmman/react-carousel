/**
  This function is the bridge to non-UI part and the redux store.
  This function check for changes in redux store only for the props mentioned in mapStateToProps parameter.
  Whenever change occurs in redux store, this function checks for changes in subscribed props and
  accordinngly calls the function specified in the mapStateToProps paramter field onPropsChange.
**/
export const subscribeToStateChanges = (store, mapStateToProps) => {

    let tempState = mapStateToProps(store.getState()).props;

    let unsubscribe = store.subscribe(() => {

        let currentStateAndCallbacks = mapStateToProps(store.getState());

        let currentState = currentStateAndCallbacks.props;

        let previousState = tempState;

        tempState = currentState;

        Object.keys(previousState).map((key)=>{
            if(previousState[key] !== currentState[key]) {
                if(Array.isArray(currentStateAndCallbacks.onPropsChange[key])){
                    currentStateAndCallbacks.onPropsChange[key].forEach(func => {func(store, currentState[key],previousState[key])})
                }
                else currentStateAndCallbacks.onPropsChange[key](store, currentState[key],previousState[key]);
            }
        });

    });
    return unsubscribe;
};

/**
  This function calls XHR Get Request.
  Just pass URL, body for XHR Call.
  Callback is when XHR has finished its execution.
  Store is redux store, which is passed when callback is called on XHR Completion.
**/
export const createXMLGetRequest = (apiURL,callback,sentData,store) => {
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200){
            let response = JSON.parse(xhr.response);
            callback({type:"success",response:response},sentData,store);
        }else if(xhr.readyState === XMLHttpRequest.DONE && xhr.status !== 200){
            callback({type:"error"},sentData,store);
        }
    };
    xhr.open("GET",apiURL,true);
    xhr.send();

    return xhr;
};
