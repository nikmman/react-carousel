import {initializeAlbuminHandlerWithStoreChanges} from './AlbumHandler';
import {initializeImagesHandlerWithStoreChanges} from './ImagesHandler';

export const initializeAllModules = (store) => {
  initializeAlbuminHandlerWithStoreChanges(store);
  initializeImagesHandlerWithStoreChanges(store);
};
