import {subscribeToStateChanges,createXMLGetRequest} from '../common/helper.js';

import {selectCallApi,selectListOfAlbums} from '../rdx/selectors/selectors.js';
import {GET_ALBUMS,GET_IMAGES_OF_ALBUM} from '../helpers/APITypes.js';
import {reportAlbums,reportApiCall} from '../rdx/actions';

const callGetAlbumsApi = (store,apiCallDetails) => {
    if(apiCallDetails) {
      createXMLGetRequest(GET_ALBUMS,responseFromGetAlbumsAPI,{},store);
    }
};

const responseFromGetAlbumsAPI = (responseData,sentData,store) => {
    if(responseData.type === 'success'){
        let response = responseData.response;
        // console.log('response get albums', response);
        store.dispatch(reportAlbums(response));
        getImagesForAlbums(store);
    }else{
        //Error
    }
};

const getImagesForAlbums = (store) => {
  let state = store.getState();
  let listOfAlbums = selectListOfAlbums(state);
  listOfAlbums.forEach((albumData,i)=> {
    let data = {
      albumId: albumData.id
    };
    store.dispatch(reportApiCall(GET_IMAGES_OF_ALBUM,data))
  });
}


const selectGetAlbumsApiCallRequired = (state) => {
    let apiRequiredToBeCalled = selectCallApi(state);

    if(apiRequiredToBeCalled.type === GET_ALBUMS){
        return apiRequiredToBeCalled;
    }
    return null;
};


const mapStateToProps = (state) => {
    return{
      props:{
        getAlbumsApiCallRequired: selectGetAlbumsApiCallRequired(state)
      },onPropsChange:{
        getAlbumsApiCallRequired: callGetAlbumsApi
      }
    }
}

export const initializeAlbuminHandlerWithStoreChanges = (store) => {
  subscribeToStateChanges(store,mapStateToProps);
};
