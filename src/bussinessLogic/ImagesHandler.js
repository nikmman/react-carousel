import {subscribeToStateChanges,createXMLGetRequest} from '../common/helper.js';
import {makeURLFromData} from '../helpers';

import {selectCallApi} from '../rdx/selectors/selectors.js';
import {GET_IMAGES_OF_ALBUM} from '../helpers/APITypes.js';
import {reportImagesOfAlbum} from '../rdx/actions';

const callGetImagesApi = (store,apiCallDetails) => {
    if(apiCallDetails) {
      let {data} = apiCallDetails;
      let url = makeURLFromData(GET_IMAGES_OF_ALBUM,data);
      createXMLGetRequest(GET_IMAGES_OF_ALBUM,responseFromGetImagesAPI,data,store);
    }
};

const responseFromGetImagesAPI = (responseData,sentData,store) => {
    if(responseData.type === 'success'){
        let response = responseData.response;
        // console.log('response get images', response);
        store.dispatch(reportImagesOfAlbum(sentData.albumId, response));
    }else{
        //Error
    }
};


const selectGetImagesApiCallRequired = (state) => {
    let apiRequiredToBeCalled = selectCallApi(state);

    if(apiRequiredToBeCalled.type === GET_IMAGES_OF_ALBUM){
        return apiRequiredToBeCalled;
    }
    return null;
};


const mapStateToProps = (state) => {
    return{
      props:{
        getImagesApiCallRequired: selectGetImagesApiCallRequired(state)
      },onPropsChange:{
        getImagesApiCallRequired: callGetImagesApi
      }
    }
}

export const initializeImagesHandlerWithStoreChanges = (store) => {
  subscribeToStateChanges(store,mapStateToProps);
};
