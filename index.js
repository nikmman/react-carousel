import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {initStore} from './src/rdx/store.js';
import App from './src/components/App.js';
import {initializeAllModules} from './src/bussinessLogic/BLModuleInitializer.js';

const store = initStore();

const initializeReactRoot = () => {
  ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    document.getElementById('react-div')
  );
};

window.store = store;
initializeAllModules(store);
initializeReactRoot();
